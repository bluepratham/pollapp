from django.shortcuts import render
from .forms import OptimForm
from .models import Optim
# Create your views here.

def index(request):
    if request.method == "GET":
        form = OptimForm()
        return render(request, 'doosan.html', {'form':form})

    if request.method == "POST":
        b = OptimForm(request.POST)
        print(request.POST)

        b.save()
        a = Optim.objects.latest('id')
        return render(request, 'doosan.html', {'yes' : 1,
                                               'mod' : a.x()})
from django.db import models
import pulp
# Create your models here.

class Optim(models.Model):
    a = models.FloatField(default=0)
    b = models.FloatField(default=0)
    c = models.FloatField(default=0)
    d = models.FloatField(default=0)
    f_a = models.FloatField(default=0)
    f_b = models.FloatField(default=0)
    def x(self):

        model = pulp.LpProblem("Profit maximising problem", pulp.LpMaximize)

        A = pulp.LpVariable('A', lowBound=0, cat='Integer')
        B = pulp.LpVariable('B', lowBound=0, cat='Integer')

        # Objective function
        model += 30000 * A + 45000 * B, "Profit"

        # Constraints
        model += 3 * A + 4 * B <= 30
        model += 5 * A + 6 * B <= 60
        model += 1.5 * A + 3 * B <= 21

        # Solve our problem
        model.solve()
        pulp.LpStatus[model.status]

        return f"A = {B.varValue}  and B = {A.varValue}"
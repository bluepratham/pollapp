from django import forms
from .models import Optim

class OptimForm(forms.ModelForm):
    class Meta:
        model = Optim
        fields = ['a','b','c','d']
